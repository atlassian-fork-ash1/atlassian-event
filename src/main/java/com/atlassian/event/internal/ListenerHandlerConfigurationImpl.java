package com.atlassian.event.internal;

import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.spi.ListenerHandler;
import com.google.common.collect.Lists;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * The default configuration that only uses the {@link com.atlassian.event.internal.AnnotatedMethodsListenerHandler}.
 * <p>
 * Products that need to remain backward compatible will have to override this configuration
 */
public class ListenerHandlerConfigurationImpl implements ListenerHandlersConfiguration {
    @Override
    @Nonnull
    public List<ListenerHandler> getListenerHandlers() {
        return Lists.newArrayList(new AnnotatedMethodsListenerHandler());
    }
}
