package com.atlassian.event.spi;

import javax.annotation.Nonnull;
import java.util.concurrent.Executor;

/**
 * A factory to create executors for asynchronous event handling
 */
public interface EventExecutorFactory {
    /**
     * @return a new {@link Executor}
     */
    @Nonnull
    Executor getExecutor();
}
