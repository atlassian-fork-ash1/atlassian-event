package it.com.atlassian.event.spring;

import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.spi.ListenerHandler;
import org.springframework.beans.factory.config.AbstractFactoryBean;

import javax.annotation.Nonnull;
import java.util.List;

public class ListenerHandlerConfigurationFactoryBean extends AbstractFactoryBean {

    private final List<ListenerHandler> listenerHandlers;

    public ListenerHandlerConfigurationFactoryBean(List<ListenerHandler> listenerHandlers) {
        this.listenerHandlers = listenerHandlers;
    }

    @Override
    public Class<?> getObjectType() {
        return ListenerHandlersConfiguration.class;
    }

    @Nonnull
    @Override
    protected ListenerHandlersConfiguration createInstance() {
        return new ListenerHandlersConfiguration() {
            @Nonnull
            public List<ListenerHandler> getListenerHandlers() {
                return listenerHandlers;
            }
        };
    }
}
